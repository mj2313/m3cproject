"""Project part 2
Use gradient thresholding to process image.
Gradient is computed using grad_omp routine in fdmodule2d
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p2 -llapack
"""
from p2 import fdmodule2d as f2d
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

def threshold(M,fac,display,savefig):
    """set all elements of M to zero where grad_amp < fac*max(grad_amp)

    In order to obtain intensity variations, fac must be a number between 0 nd 1 (including 1 but not 0). If it is non-positive, then M would stay unchange after applying tresholding condition. If it is greater than 1, M would be a zero matrix, which is also not desired. This user-friendly restriction can be achieved by using the code feature 'assert'.
    """
    assert type(fac) is int or type(fac) is float, "fac must be a number"
    assert fac>0 and fac <=1, "fac must be non-negative"
    
    f2d.dx1 = 1.
    f2d.dx2 = 1.

    f2d.n1 = M.shape[1]
    f2d.n2 = M.shape[0]
    
    dfmax = []
    for i in range(M.shape[2]):
        df1,df2,df_amp,df_max = f2d.grad_omp(M[:,:,i])
        M[:,:,i][df_amp < fac*df_max] = 0.
        dfmax += [df_max]
    
    if display:
        plt.figure()
        plt.imshow(M)
    if savefig:
        plt.savefig('p2.png')

    return M, dfmax

if __name__ == '__main__':
    M=misc.face()
    print "shape(M):",np.shape(M)
    plt.figure()
    plt.imshow(M)
    plt.show() #may need to close figure for code to continue
    N,dfmax=threshold(M,0.2,True,True)
    plt.show()
    
