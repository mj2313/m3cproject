!mpif90 -O3 -o adv_mpi.exe advection_mpi.f90
!to run mpiexec -n 2 adv_mpi.exe (using 2 processes in this example, could be any other number)
!My instructions for compiling and running the code:
!mpif90 -O3 -o ode_mpi.exe ode_mpi.f90
!mpiexec -n 2 ode_mpi.exe (using 2 processes, can be a different natural number)
module advmodule
    implicit none
    real(kind=8) :: S_adv,c_adv
end module advmodule
!-------------------------------
program advection_mpi
    use mpi
    use advmodule
    implicit none
    integer :: i1,j1
    integer :: nt,n !number of time steps, number of spatial points
    real(kind=8) :: dt,dx,pi !time step, grid size, advection eqn parameters
    integer :: myid, numprocs, ierr
    real(kind=8), allocatable, dimension(:) :: x,f0,f !grid, initial condition, solution

 ! Initialize MPI
    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)

!gather input
    open(unit=10,file='data_mpi.in')
        read(10,*) n
        read(10,*) nt
        read(10,*) dt
        read(10,*) c_adv
        read(10,*) S_adv
    close(10)

    allocate(x(n),f0(n),f(n))

!make grid from 0 to 1-dx, dx = 1/n
    do i1=1,n
        x(i1) = dble(i1-1)/dble(n)
    end do
    dx = 1.d0/dble(n)

!generate initial condition
    pi = acos(-1.d0)
    f0 = sin(2.d0*pi*x)


!compute solution
    call euler_mpi(MPI_COMM_WORLD,numprocs,n,0.d0,f0,dt,nt,dx,f)

!output solution
        call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
       if (myid==0) then
        print *, 'max(f)=',maxval(abs(f))
        open(unit=11,file='fmpi.dat')
        do i1=1,n
            write(11,*) f(i1)
        end do
        close(11)
    end if
    !can be loaded in python with: f=np.loadtxt('fmpi.dat')

    call MPI_FINALIZE(ierr)
end program advection_mpi
!-------------------------------


subroutine euler_mpi(comm,numprocs,n,t0,y0,dt,nt,dx,y)
    !explicit Euler method
    !euler_mpi(MPI_COMM_WORLD,numprocs,n,0.d0,f0,dt,nt,dx,f)
    use mpi
    use advmodule
    implicit none
    integer, intent (in) :: n,nt
    real(kind=8), dimension(n), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt,dx
    real(kind=8), dimension(n), intent(out) :: y
    real(kind=8) :: t
    integer :: i1,k,istart,iend
    integer :: comm,myid,ierr,numprocs

    !variables I added:
    integer, dimension(MPI_STATUS_SIZE) :: status
    integer :: sender, receiver, ylocsize 
    double precision, allocatable, dimension(:) :: ylocal, Rpart,  ylocal_ext
    !explanations of added variables:
        !sender and receiver are defined as in homework 5
        !ylocsize defines length of ylocal per each processor
        !ylocal is a 'subvector' of y per each processor
        !ylocal_ext is the extension of ylocal (created by prepending and appending the neighbouring entries of y in a similar way as in Part 3, task 2 (OpenMP)
        !Rpart is the output of RHS_mpi function, always with the same dimension as ylocals

    !for gather:
    integer, allocatable, dimension(:) :: Nper_proc, disps !same procedure with gatrher as in homework 5

    call MPI_COMM_RANK(comm, myid, ierr)
    print *, 'start euler_mpi, myid=',myid

    !set initial conditions
    y = y0
    t = t0

    !generate decomposition and allocate sub-domain variables
    call mpe_decomp1d(size(y),numprocs,myid,istart,iend)

    print *, 'istart,iend,threadID,npart=',istart,iend,myid

    ylocsize = iend - istart + 1
    allocate(ylocal_ext(0:ylocsize+1),ylocal(ylocsize),Rpart(ylocsize)) !set dimensions of ylocal_ext, ylocal, Rpart
    ylocal = y(istart:iend)

    !time marching
    do k = 1,nt

        !-----------------------------------------------------------
        ! Save ylocal to non-boundary elements of ylocal_ext
        !-----------------------------------------------------------
        ylocal_ext(1:ylocsize) = ylocal

        !-----------------------------------------------------------
        !Send data at top boundary up to next processor
        !i.e. send ylocal(ylocsize) to be received by myid+1 and 
        !store it there as ylocal_ext(0)
        !data from myid=numprocs-1 is sent to myid=0
        !-----------------------------------------------------------
        if (myid<numprocs-1) then
            receiver = myid+1
        else
             receiver = 0
        end if

        if (myid>0) then
            sender = myid-1
        else
            sender = numprocs-1
        end if

        call MPI_SEND(ylocal(ylocsize),1,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr) 
        call MPI_RECV(ylocal_ext(0),1,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)

        !-----------------------------------------------------------
        !Send data at bottom boundary down to previous processor
        !i.e. send ylocal(1) to myid-1 and store it there as ylocal_ext(ylocsize+1)
        !data from myid=0 is sent to myid=numprocs-1
        !-----------------------------------------------------------

        if (myid>0) then
            receiver = myid-1
        else
            receiver = numprocs-1
        end if

        if (myid<numprocs-1) then
            sender = myid+1
        else
             sender = 0
        end if

        call MPI_SEND(ylocal(1),1,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr)
        call MPI_RECV(ylocal_ext(ylocsize+1),1,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)

        call MPI_BARRIER(comm,ierr)

        call RHS_mpi(ylocsize+2,dx,t,ylocal_ext,Rpart)
        ylocal= ylocal + dt*Rpart !ylocal must be declared and defined, Rpart must be declared (DONE), and 
                                  !should be returned by RHS_mpi (DONE)
        t = t + dt
        call MPI_BARRIER(comm,ierr) 
        ! have changed MPI_COMM_WORLD to comm in the barrier (as we do not need to declare the variable and MPI_COMM_WORLD will be called by the routine itself)

    end do

    print *, 'before collection',myid, maxval(abs(ylocal))
    !collect ylocal from each processor onto myid=0
   
    allocate(Nper_proc(numprocs),disps(numprocs))

    !gather ylocsize from each proc to array Nper_proc on myid=0
    call MPI_GATHER(ylocsize,1,MPI_INT,Nper_proc,1,MPI_INT,0,comm,ierr)
    if (myid==0) then
        disps(1)=0
        do i1=2,numprocs
            disps(i1) = disps(i1-1)+Nper_proc(i1-1) !needed for gatherv below
        end do
    end if

    !collect ylocal from each processor onto myid=0
    call MPI_GATHERV(ylocal,ylocsize,MPI_DOUBLE_PRECISION,y,Nper_proc, &
                disps,MPI_DOUBLE_PRECISION,0,comm,ierr)

    if (myid==0) print *, 'finished',maxval(abs(y))

end subroutine euler_mpi
!-------------------------
subroutine RHS_mpi(nn,dx,t,f,rhs)
    use advmodule
    implicit none
    integer, intent(in) :: nn
    real(kind=8), intent(in) :: dx,t
    real(kind=8), dimension(nn), intent(in) :: f
    real(kind=8), dimension(nn-2), intent(out) :: rhs !output variable rhs must be declared (DONE)
   
    rhs = S_adv - c_adv *(0.5d0/dx)*(f(3:nn)-f(1:nn-2))
end subroutine RHS_mpi

!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D


