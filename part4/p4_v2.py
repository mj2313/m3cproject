#Project part 4
import numpy as np
import matplotlib.pyplot as plt
import adv2d

import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
#gfortran -c advmodule2d.f90
#f2py -llapack -c advmodule2d.f90 fdmoduleB.f90 fdmodule2dp.f90 ode2d.f90 -m adv2d --f90flags='-fopenmp' -lgomp
#Note:I changed ode2d.f90 to ode2d_v2.f90 in my execution command
#----------------------------------------------

def advection2d(tf,n1,n2,dx,dy,c1=1.0,c2=1.0,S=1.0,par=0,numthreads = 1, display=False):
    """
    Solves and and computes the wall-time of 2d advection equation using rk4 module from ode2d_v2,
    where the equation is: df/dt + c_1 df/dx +c_2 df/dx = S
    ,c_1,c_2 and S are constants, x=0,dx,...(n1-1)*dx, y=0,dy,...(n2-1)*dy, and f is periodic in both x and y

    Input par is identificator for parallel computation (via OpenMP) (so parallelization works only if par = 1)
    Input numthreads is the number of threads when par = 1

    Initial condition: f(x,y,t=0) = exp(-100.0*((x-x0)**2+(y-y0)**2)), where x0 = y0 = 0.5
    Returns: f(x,y,tf) , wall-time   (tf is a fixed time from the input)
    Displays a contour plot of the solution for any t if 'display=True' in the input
    """

    #construct grid and initial condition, set number of time-steps
    x = np.linspace(0,(n1-1)*dx,n1)
    y = np.linspace(0,(n2-1)*dy,n2)
    xx, yy = np.meshgrid(x, y, sparse=True)
    f0 = np.exp(-100.0*((xx-0.5)**2+(yy-0.5)**2))  #Initial condition
    nt = 16000
    dt = tf/float(nt)

    #set parallelization identificator and parameter (number of threads)
    adv2d.ode2d.par = par
    adv2d.ode2d.numthreads = numthreads
    adv2d.fdmodule2d.numthreads = numthreads

    #Set constants
    adv2d.advmodule.s_adv = S
    adv2d.advmodule.c1_adv = c1
    adv2d.advmodule.c2_adv = c2

    adv2d.fdmodule2d.n1 = n1
    adv2d.fdmodule2d.n2 = n2
    adv2d.fdmodule2d.dx1 = dx
    adv2d.fdmodule2d.dx2 = dy

    y = adv2d.ode2d.rk4(0.,f0,dt,nt)
    a = y[0]
    #display results
    if display:
        plt.figure()
        cc = plt.contour(y[0])
        matplotlib.rcParams['contour.negative_linestyle'] = 'dashed'
        plt.clabel(cc, inline=1, fontsize=10)
        plt.title('Maros Janco, advection2d advection eqn. \n solution for tf,n1,n2,dx,dy,c1,c2,S= %2.1f,%2.1f,%2.1f,%2.1f,%2.1f,%2.1f,%2.1f,%2.1f' %(tf,n1,n2,dx,dy,c1,c2,S))
    return y[0],y[1]  # solution and time


#copied text from hw4 solution, will be changed soon
#----------------------------------------------
def test_advection2d():
    """
    Call advection2d with a pre-defined range of values of n1 and n2 (n1vals = n2vals = [50, 100, 150, 200, 250, 300, 350] ) and
    parameters, tf=c1=c2=S=1, dx=1/n1, dy=1/n2
    (I did not take larger values as the computation time got too high)

    Returns:
            -m
            --  a 4 x 2 matrix of estimated convergence rates for error
            -- its 1st column are estimated convergence rates for  error when n1 varies and each row represents a different fixation of n2 = [50, 150, 250, 350]
            -- its 2nd column are estimated convergence rates for error when n2 varies and each row represents a different fixation of n1 = [50, 150, 250, 350]
            -- e.g. third row, second column of m (m[2,1] in python) is the estimated convergence rate when n2 varies and n1 = 250
            -- Note: In terms of fixation values, only every second ones of n1vals and n2vals are taken into consideration

            -mm
            -- estimated convergence rate for error when n1 = n2 = n, n varies

            error
            -- a matrix of errors for specific n1 and n2 values, where n1 varies with rows and n2 with columns, and
             n = n1 = n2 = [50, 100, 150, 200, 250, 300, 350]
            -- so, (i,j)-th entry of error is the error for n1 = i-th value of n, n2 = j-th value of n

            speedup
            -- a matrix of OpenMP speedups for specific n1 and n2 values, where n1 varies with rows and n2 with columns, and
             n = n1 = n2 = [50, 100, 150, 200, 250, 300, 350]
            -- so, (i,j)-th entry of speedup is the OpenMp speedup for n1 = i-th value of n, n2 = j-th value of n
            -- for specific n1 and n2, I ran the computation 3 times to get the averages of times (it should be done even more times
            -- (e.g. 10 as in previous homeworks) but the whole process would take much longer)
    Displays:
            1st figure
            -- four lines of error vs n1 in loglog scale, with 4 different fixations of n2 = [50, 150, 250, 350]

            2nd figure
            -- four lines of error vs n2 in loglog scale, withwhen 4 different fixations of n1 = [50, 150, 250, 350]

            3rd figure
            -- error vs n, where n = n1 = n2 in loglog scale, plus its least square linear fit

            4th figure
            -- four lines of OpenMP speedup when n1 varies, with 4 different fixations of n2 = [50, 150, 250, 350]

            5th figure
            -- four lines of OpenMP speedup when n2 varies, with 4 different fixations of n1 = [50, 150, 250, 350]

            6th figure
            -- OpenMP speedup when n varies, where n = n1 = n2, plus its least square linear fit

    Highlights to the plots obtained:
            1st figure (err_n1_varies.png)
            --  The error goes to zero very fast, already when n1 = 150

            2nd figure (err_n2_varies.png)
            --  Behaves in the same way as the 1st figure, this suggests the symmetry results associated with n1 and n2

            3rd figure (err_both_vary.png)
            -- We see that the convergence goes slower to 0 when both n1 and n2 increase at the same time, 

            4th figure (speedup_n1_varies.png),5th figure (speedup_n2_varies.png),6th figure (speedup_both_vary.png)
		    
			For the speedup figures, i think it should require bigger n1 and n2 grides, with considering more times of computing n1 and n2 so that the average time is more accurate (in this case it was only 2 because It was running very slow). The only trend I can spot is that when n=n1=n2 varies, the speedup is noticed in the beginning, then it actually gets slower around n = 150, but at n = 250 (and probably higher n1 values), the speed is again gets faster and should probably continue in such a trend.
			To sum up, I am sure that the trends would be more obvious for more denser and larger grid of n1 and n2, and considering wall-times calculated as the average of at least 10 processes for a given n1,n2 values and non-OpenMP/OpenMP method.
    """

    #set problem parameters
    tf = 1.; c1 = 1.; c2 = 1.; S = 1.;
    n1vals = np.array([50, 100, 150, 200, 250])
    n2vals= n1vals
    nn = np.size(n1vals)
    error = np.empty([nn,nn])
    speedup = np.empty([nn,nn])
    tt = np.empty(2)
    tp = np.empty(len(tt))


    for nn1 in enumerate(n1vals):
        for nn2 in enumerate(n2vals):
            #for each nn1,nn2, construct grid and homogeneous part of exact solution
            print "running with n1,n2=",nn1[1],nn2[1]
            dx = 1.0/float(nn1[1])
            dy = 1.0/float(nn2[1])
            x = np.linspace(0.0,(nn1[1]-1)*dx,nn1[1])
            y = np.linspace(0.0,(nn2[1]-1)*dy,nn2[1])
            xx,yy = np.meshgrid(x,y,sparse=True)
            f0 = np.exp(-100.0*((((xx-c1*tf)-0.5)**2+((yy-c2*tf)-0.5)**2)))

    #solve advection eqn and compute error and speedup by computing walltime average for parallel and non-parallel method over multiple (now 3) calls
            for j in range(len(tt)):
                f,tp[j] = advection2d(tf,nn1[1],nn2[1],dx,dy,c1,c2,S,1,4)   # using OpenMP with 4 threads
                tt[j] = advection2d(tf,nn1[1],nn2[1],dx,dy,c1,c2,S,0,1)[1]   # without OpenMP (1 thread), im not saving f as it should be the same as previously
            error[nn1[0],nn2[0]]= np.mean(np.abs(f-(f0+S*tf)))    #Note: analytic solution = S*tf + f0
            speedup[nn1[0],nn2[0]] = np.mean(tt)/np.mean(tp)


    #####################################################################################################
    #compute and display convergence rates:
    #####################################################################################################
    k = len(n1vals[::2])
    m = np.empty([k,2])
    p = np.empty([k,2])


    #n1 varies, n2 is fixed
    plt.figure()
    for i in range(k):
        m[i,0],p[i,0] = np.polyfit(np.log(n1vals),np.log(error[:,2*i]),1)
        plt.loglog(n1vals,error[:,2*i])
    plt.legend(('n2 = 50','150','250'),loc='best')
    plt.xlabel('n1')
    plt.ylabel('error')
    plt.title('Maros Janco, test_advection2d, variation of error with \nn2 (n1 is fixed) for tf,c1,c2,S = %2.1f,%2.1f,%2.1f,%2.1f' %(tf,c1,c2,S))
    plt.axis('tight')
    plt.savefig('err_n1_varies.png')

    #n2 varies, n1 is fixed
    plt.figure()
    for i in range(k):
        m[i,1],p[i,1] = np.polyfit(np.log(n2vals),np.log(error[2*i,:]),1)
        plt.loglog(n1vals,error[2*i,:])
    plt.legend(('n1 = 50','150','250'),loc='best')
    plt.xlabel('n2')
    plt.ylabel('error')
    plt.title('Maros Janco, test_advection2d, variation of error with \nn1 (n2 is fixed) for tf,c1,c2,S= %2.1f,%2.1f,%2.1f,%2.1f' %(tf,c1,c2,S))
    plt.axis('tight')
    plt.savefig('err_n2_varies.png')


    #n1=n2=n, n varies
    mm,pp = np.polyfit(np.log(n1vals),np.log(np.diag(error)),1)
    #display this case
    plt.figure()
    plt.loglog(n1vals,np.diag(error))
    plt.loglog(n1vals,np.exp(pp)*n1vals**mm,'--')
    plt.legend(('computed','lsq linear fit'),loc='best')
    plt.xlabel('n')
    plt.ylabel('error')
    plt.title('Maros Janco, test_advection2d, variation of error with \nn = n1 = n2 for tf,c1,c2,S= %2.1f,%2.1f,%2.1f,%2.1f' %(tf,c1,c2,S))
    plt.axis('tight')
    plt.savefig('err_both_vary.png')

    #####################################################################################################
    #display speedup:
    #####################################################################################################

    #n1 varies, n2 is fixed
    plt.figure()
    for i in range(k):
        plt.plot(n1vals,speedup[:,2*i])
    plt.legend(('n2 = 50','150','250'),loc='best')
    plt.xlabel('n1')
    plt.ylabel('speedup')
    plt.title('Maros Janco, test_advection2d, speedup of rk4 with parallelized RHS \n(to non-parallelized RHS) with n2 (n1 is fixed) for tf,c1,c2,S= %2.1f,%2.1f,%2.1f,%2.1f' %(tf,c1,c2,S))
    plt.axis('tight')
    plt.savefig('speedup_n1_varies.png')

    #n2 varies, n1 is fixed
    plt.figure()
    for i in range(k):
        plt.plot(n1vals,speedup[2*i,:])
    plt.legend(('n1 = 50','150','250'),loc='best')
    plt.xlabel('n2')
    plt.ylabel('speedup')
    plt.title('Maros Janco, test_advection2d, speedup of rk4 with parallelized RHS \n(to non-parallelized RHS) with n1 (n2 is fixed) for tf,c1,c2,S= %2.1f,%2.1f,%2.1f,%2.1f' %(tf,c1,c2,S))
    plt.axis('tight')
    plt.savefig('speedup_n2_varies.png')


    #n1=n2=n, n varies
    plt.figure()
    plt.plot(n1vals,np.diag(speedup))
    plt.legend(('computed','lsq linear fit'),loc='best')
    plt.xlabel('n')
    plt.ylabel('speedup')
    plt.title('Maros Janco, test_advection2d, speedup of rk4 with parallelized RHS \n(to non-parallelized RHS) with n = n1 = n2 for tf,c1,c2,S= %2.1f,%2.1f,%2.1f,%2.1f' %(tf,c1,c2,S))
    plt.axis('tight')
    plt.savefig('speedup_both_vary.png')

    return -m,-mm,error,speedup

if __name__=='__main__':
    #add code as needed

    n1 = 100
    n2 = 100
    dx = 1.0/float(n1)
    dy = 1.0/float(n2)
    tf = 2.

    #f,t = advection2d(tf,n1,n2,dx,dy,display=True)
    #print f.shape
    #plt.show()

    conv_rates_onevaries, conv_rates_bothvary, error, speedup = test_advection2d()
    print conv_rates_onevaries, conv_rates_bothvary, error, speedup
