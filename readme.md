#### The repo should also contain a readme file with a list of all files in the repo that you created or modified and a very brief description of each file.

### Remarks:
#### I am not including files with .so, .mod, .o suffix in my list as these are just compiled modules necessary to run the codes and their compilation instructions are already written in the beginning of relevant files
####  Whenever a second version was updated (like ode2d_v2 etc.), I was modifying and working with only that one (thus first versions will not be shown).

#Folders and their files in my m3c 
project:

##Part 1
####	fdmodule.f90
#####   - module containing routines for 1D 2nd order and 4th order-compact finite differences
#####   - test routine test_fd applies these methods to a Gaussian function and
#####    returns the error while test_fd_time also returns timing information
####    fdmodule2d.f90 
######  (copied from HW 4 Solutions)
#####   - contains routines for 2D 2nd order and 4th order compact finite differences
#####   - test routines apply these methods to a simple test function and
#####     return the error and time
####	p1_3.py
#####   - test gradient routines in fdmodule2d
####	hw411.png
#####   - the output from p1_3.py
####	hw412.png
#####   - the output from p1_3.py

##Part 2
####    fdmodule2d.f90 
######  (copied from HW 4 Solutions)
#####   - contains routines for 2D 2nd order and 4th order compact finite differences
#####   - test routines apply these methods to a simple test function and
#####     return the error and time
####	p2.py
#####   - uses gradient thresholding to process image, 
#####     where gradient is computed using grad_omp routine in fdmodule2d
####	p2.png
#####   - the output gradient-thresholded image from p2.py
 
##Part 3
####	advmodule.f90
######  (not assessed)
#####   - contains parameters for advection equation and its main program tests via Euler
####	ode_v2.f90
#####   - module to use RK4 or Euler time marching to solve an initial value problem
#####   - Solves: dy/dt = RHS(y,t)
####	ode_mpi.f90
#####   - MPI version of ode_v2
####    fmpi.dat
#####   - output data from ode_mpi.f90
####	p3_v2.py
#####   - solves the 1d advection equation in fortran
####    data.in
######  (not assessed)
#####   - input parameters for a test program in avdmodule.f90
####    data_mpi.in
#####   - input parameters for ode_mpi.f90
####    test_mpi.py
#####   - code for plotting the fmpi.dat from ode_mpi.f90
####	test_mpi.png
#####   - the output graph from test_mpi.py
 
##Part 4
####    fdmodule2d.f90 
######  (copied from HW 4 Solutions)
####    fdmodule2dp.f90 
######  - simplified version of fdmodule2d.f90 (fd2 is used instead of cfd4, df_amp and df_max are disregarded)
####	ode2d.f90
######  - module to use RK4 time marching to solve an initial value problem
######  - Solves: dy/dt = RHS(y,t) where y is a 2d matrix
####	p4.py
#####   - assesses accuracy and speedup of RK4 time marching from ode2d.f90
####	err_n1_varies.png
######  - four lines of error vs n1 in loglog scale, with 4 different fixations of n2 = [50, 150, 250]
####	err_n2_varies.png
######  - four lines of error vs n2 in loglog scale, withwhen 4 different fixations of n1 = [50, 150, 250]
####	err_both_vary.png
######  - error vs n, where n = n1 = n2 in loglog scale, plus its least square linear fit
####	speedup_n1_varies.png
######  - four lines of OpenMP speedup when n1 varies, with 4 different fixations of n2 = [50, 150, 250]
####	speedup_n2_varies.png
######  - four lines of OpenMP speedup when n2 varies, with 4 different fixations of n1 = [50, 150, 250]
####	speedup_both_vary.png
######  - OpenMP speedup when n varies, where n = n1 = n2, plus its least square linear fit


